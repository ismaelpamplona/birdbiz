import axios from "axios";
const apiUrl = "https://birdbiz-api.herokuapp.com/";
// const apiUrl = "http://localhost:8888/";
import store from "../store";

export function* loginRequest(user) {
    const url = apiUrl + "user/login";
    yield axios
        .post(url, user)
        .then(function(response) {
            const user = {
                email: JSON.parse(response.config.data).email,
                image: JSON.parse(response.config.data).image,
                token: response.data.token
            };
            store.dispatch({
                type: "LOGIN_USER",
                payload: user
            });
            store.dispatch({
                type: "REMOVE_ALL_MSGS"
            });
            window.open("#/home", "_self");
        })
        .catch(function(error) {
            const msg = {
                status: error.response.status,
                data: [error.response.data.message]
            };
            store.dispatch({
                type: "SHOW_POST_STATUS_MSG",
                payload: msg
            });
        });
}

export function* singUp(user) {
    const url = apiUrl + "user/singup";
    yield axios
        .post(url, user)
        .then(function(response) {
            const createdUser = {
                email: response.data.result.email,
                token: response.data.token
            };
            store.dispatch({
                type: "LOGIN_USER",
                payload: createdUser
            });
            store.dispatch({
                type: "REMOVE_ALL_MSGS"
            });
            window.open("#/home", "_self");
        })
        .catch(function(error) {
            const msg = {
                status: error.response.status,
                data: [error.response.data.error.message]
            };
            store.dispatch({
                type: "SHOW_POST_STATUS_MSG",
                payload: msg
            });
        });
}

export function* isLoggedIn(token) {
    axios({
        method: "post",
        url: apiUrl + "user/isLoggedIn/",
        headers: {
            Authorization: token
        }
    })
        .then(function(response) {})
        .catch(function(err) {});
}

export function* forgotRequest(email) {
    const url = apiUrl + "user/forgot";
    let msg = {};
    yield axios
        .post(url, email)
        .then(function(response) {
            msg = {
                status: response.status,
                data: [response.data.message]
            };
        })
        .catch(function(error) {
            msg = {
                status: error.response.status,
                data: [error.response.data.error.message]
            };
        });
    store.dispatch({
        type: "SHOW_POST_STATUS_MSG",
        payload: msg
    });
}

export function* resetRequest(resetUserData) {
    const url = apiUrl + "user/reset";
    let msg = {};
    yield axios
        .post(url, resetUserData)
        .then(function(response) {
            msg = {
                status: response.status,
                data: ["Password updated! 🤓 "]
            };
            window.open("#/login", "_self");

            setTimeout(function() {
                store.dispatch({
                    type: "SHOW_POST_STATUS_MSG",
                    payload: msg
                });
            }, 120);
        })
        .catch(function(error) {
            msg = {
                status: error.response.status,
                data: [error.response.data.error.message]
            };
            store.dispatch({
                type: "SHOW_POST_STATUS_MSG",
                payload: msg
            });
        });
}
