// JS
import "../build/tags";
import "./components/app";
import store from "./store";

// JS Libraries
import riot from "riot";

// Execute
const reduxGlobal = { ...store };
riot.mixin("reduxGlobal", reduxGlobal);
riot.mount("#view", "app");
