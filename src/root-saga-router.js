import store from "./store";
import route from "riot-route";
import catchErrors from "./handlers/error-handler";
import { takeEvery, call, all } from "redux-saga/effects";
import * as appController from "./app-controller";
import axios from "axios";
import formSaga from "./components/forms/form-saga";

const apiUrl = "https://birdbiz-api.herokuapp.com/";
// const apiUrl = "http://localhost:8888/";

const executeRoute = (...params) => {
    const token = "Bearer " + store.getState().appReducer.user.token;
    const query = route.query();
    // If query is an empty object, return original params
    // If query exists, remove the last params item (that is the query)
    if (Object.keys(query).length !== 0) {
        params.pop();
    }

    if (
        params[0] === "login" ||
        params[0] === "singup" ||
        params[0] === "forgot" ||
        params[0] === "reset"
    ) {
        return store.dispatch({
            type: "ROUTE",
            payload: {
                params,
                query
            }
        });
    }
    axios({
        method: "post",
        url: apiUrl + "user/isLoggedIn/",
        headers: {
            Authorization: token
        }
    })
        .then(function(response) {
            store.dispatch({
                type: "ROUTE",
                payload: {
                    params,
                    query
                }
            });
        })
        .catch(function(err) {
            window.open("#/login", "_self");
            store.dispatch({
                type: "TURN_LOGIN_PAGE_ON"
            });
        });
};

// start and configure riot-route
route.base("#/");
route(executeRoute); // for each route change, run executeRoute function above who will dispatch an action with the type 'ROUTE'
route.start(true);

function* handleRoute(action) {
    switch (action.payload.params[0]) {
        case "login": {
            yield call(appController.loading);
            yield call(appController.login, action);
            break;
        }
        case "singup": {
            yield call(appController.loading);
            yield call(appController.singup, action);
            break;
        }
        case "reset": {
            yield call(appController.loading);
            yield call(appController.reset, action);
            break;
        }
        case "forgot": {
            yield call(appController.loading);
            yield call(appController.forgot, action);
            break;
        }
        case "home": {
            yield call(appController.loading);
            yield call(appController.home, action);
            break;
        }
        case "page1": {
            yield call(appController.loading);
            yield call(appController.pageOne, action);
            break;
        }
        case "notfound": {
            yield call(appController.loading);
            yield call(appController.notfound, action);
            break;
        }
        default: {
            throw 404;
        }
    }
}

function* sagaRoute() {
    yield takeEvery("ROUTE", catchErrors, handleRoute); // for every dispatch with the type ROUTE will run the handleRoute function
}

export default function* rootSaga() {
    // yield takeEvery("ROUTE", catchErrors, handleRoute);
    yield all([call(sagaRoute), call(formSaga)]);
}
