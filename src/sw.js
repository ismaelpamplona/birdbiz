// this file sets the SERVICE WORKER for a Progressive Web Application - PWA

console.log("inside the sw.js file");

self.addEventListener("install", function(e) {
    console.log("[Service Worker] Installing Service Worker ...", e);
    e.waitUntil(
        caches.open("static").then(function(cache) {
            console.log("[Service Worker] Precaching App Shell");
            cache.addAll([
                "/",
                "/index.html",
                "/bundle.js",
                "/main.css",
                "/fonts/a67ee72b54a03d425eb928e92702f54f.ttf",
                "/img/user.png",
                "/img/birdbiz-logo.gif",
                "/img/spinner.gif"
            ]);
        })
    );
});

self.addEventListener("activate", function(e) {
    console.log("[Service Worker] Activating Service Worker ...", e);
    return self.clients.claim;
});

self.addEventListener("fetch", function(e) {
    e.respondWith(
        caches.match(e.request).then(function(res) {
            if (res) {
                return res;
            } else {
                return fetch(e.request);
            }
        })
    );
});

// https://httpbin.org/ -  A simple HTTP Request & Response Service.
fetch("https://httpbin.org/ip")
    .then(function(res) {
        return res.json();
    })
    .then(function(data) {
        console.log(data);
    })
    .catch(function(err) {
        console.log(err);
    });
