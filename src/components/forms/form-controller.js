import { put } from "redux-saga/effects";

export function* showMsg(action) {
    yield put({
        type: "SHOW_POST_STATUS_MSG",
        payload: action
    });
}
