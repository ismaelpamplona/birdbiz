import store from "../../store";
import { takeLatest, call, all } from "redux-saga/effects";
import * as formController from "./form-controller";
import * as appController from "../../app-controller";
import * as api from "../../handlers/api-handler";
import riot from "riot";

const listenData = data => {
    // console.log(data);
};

const clearForm = () => {
    const allForms = document.querySelectorAll(".form");
    allForms.forEach(form => {
        form.reset();
    });
};

const clearFormCancelBtn = () => {
    clearForm();
    store.dispatch({
        type: "REMOVE_ALL_MSGS"
    });
};

const removeMsg = data => {
    store.dispatch({
        type: "REMOVE_MSG",
        payload: data
    });
};

const executeRegister = data => {
    store.dispatch({
        type: "POST_USER",
        payload: { data }
    });
};

function* handleRegister(action) {
    yield call(appController.loading);
    yield api.singUp(action.payload.data);
    yield call(appController.loading);
}

const executeLogin = data => {
    store.dispatch({
        type: "LOGIN_REQUEST",
        payload: { data }
    });
};

function* handleLogin(action) {
    yield call(appController.loading);
    yield api.loginRequest(action.payload.data);
    yield call(appController.loading);
}

const executeLogout = () => {
    store.dispatch({
        type: "LOGOUT_USER"
    });
};

const executeReset = data => {
    store.dispatch({
        type: "RESET_REQUEST",
        payload: { data }
    });
};

function* handleReset(action) {
    yield call(appController.loading);
    yield api.resetRequest(action.payload.data);
    yield call(appController.loading);
}

const executeForgot = data => {
    store.dispatch({
        type: "FORGOT_REQUEST",
        payload: { data }
    });
};

function* handleForgot(action) {
    yield call(appController.loading);
    yield api.forgotRequest(action.payload.data);
    yield call(appController.loading);
    clearForm();
}

export default function* formSaga() {
    yield all([
        yield takeLatest("POST_USER", handleRegister),
        yield takeLatest("LOGIN_REQUEST", handleLogin),
        yield takeLatest("RESET_REQUEST", handleReset),
        yield takeLatest("FORGOT_REQUEST", handleForgot)
    ]);
}

// passing riot mixin to the component
riot.mixin("formSagaMethods", {
    executeRegister,
    executeLogin,
    executeLogout,
    executeForgot,
    executeReset,
    listenData,
    removeMsg,
    clearForm,
    clearFormCancelBtn
});
