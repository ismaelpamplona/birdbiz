import "./forms.scss";
import riot from "riot";

import logoNav from "../../img/birdbiz-logo.gif";

import userIcon from "../../img/user.png";

window.addEventListener("keydown", function(e) {
    e.key === "Enter" && e.preventDefault();
});

riot.mixin("login-form", {
    logoNav,
    userIcon
});
