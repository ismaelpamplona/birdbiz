// importing components / tags
import "./navbar/navbar";
import "./home/home";
import "./page1/page1";
import "./footerbar/footerbar";
import "./searchbar/searchbar";
import "./notfound/notfound";
import "./submenu/submenu";
import "./forms/forms";
import "./spinner/spinner";
import "./sidebar/sidebar";
import "./notifications-link/notifications-link";

// importing styling files
import "./app.scss";

// importing favicon
import "../img/birdbiz-favicon.png";
import "../img/app-icon-48x48.png";
import "../img/app-icon-96x96.png";
import "../img/app-icon-144x144.png";
import "../img/app-icon-192x192.png";
import "../img/app-icon-256x256.png";
import "../img/app-icon-384x384.png";
import "../img/app-icon-512x512.png";

// importing servive worker for pwa - if current browser supports
if ("serviceWorker" in navigator) {
    console.log("navigator has service worker");

    navigator.serviceWorker.register("./sw.js").then(function() {
        console.log("Service Worker registered");
    });
}
