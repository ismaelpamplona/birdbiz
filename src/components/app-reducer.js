import md5 from "md5";
import userAvatarPath from "../img/user.png";

const initialState = {
    urlApi: "https://birdbiz-api.herokuapp.com/",
    view: { isSpinnerVisible: false },
    msg: { data: [], status: "" },
    user: { avatar: userAvatarPath }
};

export default function appReducer(state = initialState, action) {
    switch (action.type) {
        case "TURN_ALL_PAGES_OFF": {
            const view = {};
            const msg = { data: [], status: "" };
            return { ...state, view, msg };
        }
        case "TURN_LOGIN_PAGE_ON": {
            const view = {
                ...state.view,
                isLoggedIn: false,
                showLogin: true,
                showSingup: false,
                showForgot: false,
                showReset: false,
                firstNavClass: "main-nav-login",
                authFlowContentClass: "login-content"
            };
            const user = { avatar: userAvatarPath };
            return { ...state, view, user };
        }
        case "TURN_SINGUP_PAGE_ON": {
            const view = {
                ...state.view,
                isLoggedIn: false,
                showLogin: false,
                showSingup: true,
                showForgot: false,
                showReset: false,
                firstNavClass: "main-nav-singup",
                authFlowContentClass: "singup-content"
            };
            const user = { avatar: userAvatarPath };
            return { ...state, view, user };
        }
        case "TURN_FORGOT_PAGE_ON": {
            const view = {
                ...state.view,
                isLoggedIn: false,
                showLogin: false,
                showSingup: false,
                showForgot: true,
                showReset: false,
                firstNavClass: "main-nav-forgot",
                authFlowContentClass: "forgot-content"
            };
            const user = { avatar: userAvatarPath };
            return { ...state, view, user };
        }
        case "TURN_RESET_PAGE_ON": {
            const view = {
                ...state.view,
                isLoggedIn: false,
                showLogin: false,
                showSingup: false,
                showForgot: false,
                showReset: true,
                firstNavClass: "main-nav-reset",
                authFlowContentClass: "reset-content"
            };
            const user = { avatar: userAvatarPath };
            return { ...state, view, user };
        }
        case "LOGIN_USER": {
            const view = {
                ...state.view,
                isLoggedIn: true
            };

            const hash = md5(action.payload.email);
            const gravatar = `https://gravatar.com/avatar/${hash}/?d=retro`;

            const user = {
                avatar: gravatar,
                email: action.payload.email,
                token: action.payload.token
            };
            return { ...state, view, user: user };
        }
        case "LOGOUT_USER": {
            const view = {
                ...state.view,
                isLoggedIn: false
            };
            const avatar = userAvatarPath;
            return { ...state, view, user: {} };
        }
        case "LOADING": {
            const view = {
                ...state.view,
                isSpinnerVisible: !state.view.isSpinnerVisible
            };
            return { ...state, view };
        }
        case "SHOW_POST_STATUS_MSG": {
            const view = {
                ...state.view
            };

            const msg = {
                status: action.payload.status,
                data: action.payload.data
            };
            return { ...state, view, msg };
        }
        case "REMOVE_ALL_MSGS": {
            const msg = { data: [], status: "" };
            return { ...state, msg };
        }
        case "REMOVE_MSG": {
            let msg = { ...state.msg };
            msg.data.splice(action.payload, 1);
            return { ...state, msg };
        }
        case "TURN_HOME_PAGE_ON": {
            const view = {
                ...state.view,
                isHomeVisible: true,
                isLoggedIn: true,
                isSidebarrVisible: true
            };
            return { ...state, view };
        }
        case "TURN_PAGE1_ON": {
            const view = {
                ...state.view,
                isPageOneVisible: true,
                isLoggedIn: true,
                isSidebarrVisible: true
            };
            return { ...state, view };
        }
        case "TURN_NOTFOUND_ON": {
            console.log("TURN_NOTFOUND_ON");
            const view = {
                ...state.view,
                isLoggedIn: true,
                isPageNotFoundVisible: true
            };
            return { ...state, view };
        }
        default: {
            return state;
        }
    }
}
