import riot from "riot";
import "./notfound.scss";

import error404 from "../../img/error-404.png";

riot.mixin("notfound", {
    error404
});
