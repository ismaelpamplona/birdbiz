import riot from "riot";
import "./navbar.scss";

import logoNav from "../../img/birdbiz-logo.gif";
import toggleGo from "../../img/toggle_go.gif";
import toggleBack from "../../img/toggle_back.gif";
import toggleStatic from "../../img/toggle_static.png";

const companies = {
    title: "Companies",
    type: "companies",
    links: [
        { link: "Bird Invest", href: "bird-invest" },
        { link: "Bird Insurance", href: "bird-insurance" },
        { link: "Bird School", href: "bs" }
    ]
};

const notifications = {
    title: "Notifications",
    type: "notifications",
    links: [
        { link: "Message from Oreia Trader" },
        { link: "Message from Insurance Office" },
        { link: "Message from Bird School Leader" }
    ]
};

const user = {
    title: "Ismael Pamplona",
    type: "user right-sub", // right-sub is a class that you need to pass when your submenu is in the right position
    links: [
        { link: "Profile", href: "profile" },
        { link: "Settings", href: "settings" },
        { link: "Sign out", href: "logout" }
    ]
};

const sub = {
    title: "Sub Title",
    type: "sub",
    links: [
        { link: "Sub-menu 1" },
        { link: "Sub-menu 2" },
        { link: "Sub-menu 3" }
    ]
};

riot.mixin("nav", {
    logoNav,
    toggleGo,
    toggleBack,
    toggleStatic,
    companies,
    user,
    sub,
    notifications
});
