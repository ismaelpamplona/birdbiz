import appReducer from "./components/app-reducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
    appReducer
});

export default rootReducer;
