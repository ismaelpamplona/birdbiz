import { put } from "redux-saga/effects";

export function* loading() {
    yield put({
        type: "LOADING"
    });
}

export function* login(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_LOGIN_PAGE_ON"
    });
}

export function* singup(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_SINGUP_PAGE_ON"
    });
}

export function* reset(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_RESET_PAGE_ON"
    });
}

export function* forgot(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_FORGOT_PAGE_ON"
    });
}

export function* home(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_HOME_PAGE_ON"
    });
}

export function* pageOne(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_PAGE1_ON"
    });
}

export function* notfound(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_NOTFOUND_ON"
    });
}
